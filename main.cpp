#include <iostream>
#include <chrono>
#include "algorithms.h"

#define LO -1000
#define HI 1000
using namespace std;
using namespace std::chrono;

vvd random_matrix(int m, int n){
    vvd new_matrix(m, vd(n));
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            new_matrix[i][j] = LO + static_cast <double> (rand()) /( static_cast <double> (RAND_MAX/(HI-LO)));
        }
    }
    return new_matrix;
}



int main() {
    vvd matrix_A = random_matrix(1024, 1024);
    vvd matrix_B = random_matrix(1024,1024);

    auto start = high_resolution_clock::now();
    vvd C_classic = classic_mul(matrix_A, matrix_B);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<seconds>(stop - start);
    cout << "Time taken by classic: "
         << duration.count() << " seconds" << endl;

    start = high_resolution_clock::now();
    vvd C_strassen = strassen(matrix_A, matrix_B);
    stop = high_resolution_clock::now();
    duration = duration_cast<seconds>(stop - start);
    cout << "Time taken by strassen: "
         << duration.count() << " seconds" << endl;
    cout << "Are all consist of same values? " << are_same(C_classic, C_strassen) << endl;
    return 0;
}
