//
// Created by Nadav on 28/01/2022.
//

#ifndef MATMUL_FASTER_CPP_TYPEDEFS_H
#define MATMUL_FASTER_CPP_TYPEDEFS_H

#include <vector>

using namespace std;

typedef vector<double> vd;
typedef vector<vd> vvd;

#endif //MATMUL_FASTER_CPP_TYPEDEFS_H
