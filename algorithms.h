//
// Created by Nadav on 28/01/2022.
//

#ifndef MATMUL_FASTER_CPP_ALGORITHMS_H
#define MATMUL_FASTER_CPP_ALGORITHMS_H

#include "typedefs.h"

vvd classic_mul(const vvd& A, const vvd& B);

vvd strassen(const vvd& A, const vvd& B);

bool are_same(const vvd& A, const vvd& B, double EPS=1e-6);

#endif //MATMUL_FASTER_CPP_ALGORITHMS_H
