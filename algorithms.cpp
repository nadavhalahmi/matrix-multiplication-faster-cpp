//
// Created by Nadav on 28/01/2022.
//

#include <stdlib.h>
#include "algorithms.h"

using namespace std;

vvd operator+(const vvd& A, const vvd& B)
{
    vvd C = vvd(A.size(), vd(A[0].size()));
    for(int i=0;i<A.size();i++){
        for(int j=0;j<A[0].size();j++){
            C[i][j] = A[i][j] + B[i][j];
        }
    }
    return C;
}

vvd operator-(const vvd& A, const vvd& B){
    vvd C = vvd(A.size(), vd(A[0].size()));
    for(int i=0;i<A.size();i++){
        for(int j=0;j<A[0].size();j++){
            C[i][j] = A[i][j] - B[i][j];
        }
    }
    return C;
}

bool are_same(const vvd& A, const vvd& B, double EPS){
    for(int i=0;i<A.size();i++){
        for(int j=0;j<A[0].size();j++){
            if(abs(A[i][j] - B[i][j]) > EPS) {
                return false;
            }
        }
    }
    return true;
}

vvd classic_mul(const vvd& A, const vvd& B){
    vvd C = vvd(A.size(), vd(B.size(),0));
    for(int i=0;i<A.size(); i++){
        for(int j=0;j<B[0].size(); j++){
            for(int k=0;k<A[0].size();k++){
                C[i][j] += A[i][k] * B[k][j];
            }
        }
    }
    return C;
}

void slice(const vvd& orig, vvd& sliced_orig, int r0, int r1, int c0, int c1){
    sliced_orig.reserve(r1-r0);
    for (size_t i = r0; i < r1; ++i) {
        sliced_orig.emplace_back(orig[i].begin() + c0, orig[i].begin() + c1);
    }
}

void split(const vvd& A, vvd& A11, vvd& A12, vvd& A21, vvd& A22){
    int row2 = A.size()/2;
    int col2 = A[0].size()/2;
    slice(A, A11, 0, row2, 0, col2);
    slice(A, A12, 0, row2, col2, A[0].size());
    slice(A, A21, row2, A.size(), 0, col2);
    slice(A, A22, row2, A.size(), col2, A[0].size());
}

vvd combine(const vvd& A11, const vvd& A12, const vvd& A21, const vvd& A22){
    vvd A(A11.size()+A12.size(), vd(A11[0].size()+A21[0].size()));
    for (size_t i = 0; i < A11.size(); ++i) {
        for(size_t j=0; j<A11[0].size();j++){
            A[i][j] = A11[i][j];
        }
        for(size_t j=0; j<A11[0].size();j++){
            A[i][j+A11[0].size()] = A12[i][j];
        }
    }
    for (size_t i = 0; i < A21.size(); ++i) {
        size_t A_pos = i+A11.size();
        for(size_t j=0; j<A21[0].size();j++){
            A[A_pos][j] = A21[i][j];
        }
        for(size_t j=0; j<A21[0].size();j++){
            A[A_pos][j+A21[0].size()] = A22[i][j];
        }
    }
    return A;
}

vvd strassen(const vvd& A, const vvd& B) {

// Base case when size of matrices is 1x1
    if(A.size() == 1)
        return {1,vd(1,A[0][0]*B[0][0])};

// Splitting the matrices into quadrants. This will be done recursively
// until the base case is reached.
    vvd A11, A12, A21, A22;
    vvd B11, B12, B21, B22;
    split(A, A11, A12, A21, A22);
    split(B, B11, B12, B21, B22);


// Computing the 7 products, recursively (p1, p2...p7)
    vvd M1, M2, M3, M4, M5, M6, M7;
    M1 = strassen(A11 + A22, B11 + B22);
    M2 = strassen(A21 + A22, B11);
    M3 = strassen(A11, B12 - B22);
    M4 = strassen(A22, B21 - B11);
    M5 = strassen(A11 + A12, B22);
    M6 = strassen(A21 - A11, B11 + B12);
    M7 = strassen(A12 - A22, B21 + B22);

// Computing the values of the 4 quadrants of the final matrix c
    vvd C11, C12, C21, C22;
    C11 = M1 + M4 - M5 + M7;
    C12 = M3 + M5;
    C21 = M2 + M4;
    C22 = M1 - M2 + M3 + M6;

// Combining the 4 quadrants into a single matrix by stacking horizontally and vertically.
    vvd C = combine(C11, C12, C21, C22);
    return C;
}